const express = require('express');
const app = express();
const port = 3000;

app.get('/',(req, res)=>{
    let today = new Date();
    res.json({
        message: `Xin chào! Hôm nay là ngày ${today.getDate()}`
    })
})

app.listen(port, ()=> {
    console.log(`App listenning on port ${port}`);
})